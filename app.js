const http = require("http")
const Todo = require("./serverFunctions")
const reqData = require("./reqData")


//Get method to get all tasks
const app = http.createServer(async (req, res) => {
    if (req.url === "/todos" && req.method ==="GET") {
        try {
            const todos = await new Todo().getAllTodos()
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify(todos))
        } catch (error) {
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message:error}))
        }


        //Get method to get single task
    } else if (req.url.match(/\/todos\/([0-5]+)/) && req.method ==="GET") {
        try {
            // console.log(req.url.split('/'));
            const id = req.url.split('/')[2];
            const todo = await new Todo().getTodo(id)
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify(todo))
        } catch (error) {
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message:error}))
        }


        //POST method
    } else if (req.url==='/todos' && req.method ==="POST") {
        try {
            const todoData = await reqData(req)
            const todo = await new Todo().createNewTodo(JSON.parse(todoData))
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify(todo))
        } catch (error) {
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: error}))
        }
        

        //PATCH method to update single task
    } else if (req.url.match(/\/todos\/([0-5]+)/) && req.method ==="PATCH") {
        try {
            const id = req.url.split('/')[2];
            const updatedTodo = await new Todo().updateTodo(id)
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify(updatedTodo))
        } catch (error) {
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: error.message}))
        }

        
        //PUT method to Update a task
    } else if (req.url.match(/\/todos\/([0-5]+)/) && req.method ==="PUT") {
        try {
            const id = req.url.split('/')[2];
            const updatedTodo = await new Todo().updateTodo(id)
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify(updatedTodo))
        } catch (error) {
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: error.message}))
        }
        

        //Delete method to Update a task
    } else if (req.url.match(/\/todos\/([0-5]+)/) && req.method ==="DELETE") {
        try {
            const id = req.url.split('/')[2];
            const message = await new Todo().deleteTodo(id)
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify(message))
        } catch (error) {
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: error.message}))
        }


    } else {
        res.writeHead(404, {"Content-Type": "application/json"})
        res.end(JSON.stringify({message: "Route is not found"}))
    }
})


app.listen(8000, () => console.log("Server is started listening on port 8000"));
