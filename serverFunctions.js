const data = require("./data.json")

class serverFunctions {

    // For all tasks
    
    async getAllTodos() {
        return new Promise((resolve,reject) => {
            if(data.length === 0)
            { 
                reject("No data Found") 
            }
            else{ 
                resolve(data) 
            }
        })
    }

    //For single task

    async getTodo(id) {
        return new Promise((resolve,reject) => {
            const todo = data.find(todo=>todo.id === parseInt(id))
            if (todo) {
                resolve(todo)
            } else {
                reject("Task with given id not found")
            }
        })
    }

    //  Creating a new task

    async createNewTodo(todo) {
        return new Promise((resolve,reject) => {
            const newTodo = {
                id : Math.floor(30 + Math.random()*100),
                ...todo
            }
            
            resolve(newTodo)
        })
    }

    //Updating a task

    async updateTodo(id) {
        return new Promise((resolve,reject)=>{
            const todo = data.find(todo=>todo.id===parseInt(id))
            if (!todo) {
                reject("Task with given id not found")
            } else {
                todo["completed"] = true
                resolve(todo)
            }
        })
    }

    //Deleting a task

    async deleteTodo(id) {
        return new Promise((resolve,reject)=>{
            const todo = data.find(todo=>todo.id===parseInt(id))
            if (!todo) {
                reject("Task with given id not found") 
            } else {
                resolve(`Task deleted successfully`)
            }
        })
    }

}

module.exports = serverFunctions
